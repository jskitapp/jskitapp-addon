## jskit ui api

一个 [JSKit UI API](https://jskitapp.com/api/ui-api.html) 的封装，更易用。

源码：[jsk-ui.js](./jsk-ui.js) 

如果你对此代码有优化建议，欢迎提交 PR 到 [https://github.com/jskitapp/jskitapp-addon][1]，欢迎与我联系！


例如显示一个Hello World：

```js
import * as ui from "ui";
import {loadPage, addPage, TextNode, VPage} from "jsk-ui";

const text = new TextNode();

text.setText("Hello World");
text.setStyle({
    flexGrow: 1,
});
text.setTextSize('17sp');
text.setGravity('center');
text.setOnTap(() => {
    text.setText("Hello JSKit!");
    ui.toast("Hello JSKit!");
});

const page = new VPage();
page.appendChild(text);

addPage(page);

loadPage(page);
```

[1]: https://github.com/jskitapp/jskitapp-addon
