// es module, 对 jskitapp.com ui api 的进一步抽象封装。
// 欢迎提交 PR 到 github.com/jskitapp/jskitapp-addon
// author: xyzxqs

import * as ui from 'ui';

//为了让敲代码更快一点，整个别名。
const genId = ui.generateId;

// JSKit UI 框架只会序列化对象 enumerable 的 prop 数据
// 约定：以_开头的方法为内部方法，使用者使用时必须知道在干什么

function _defineProp(target, name, value, enumerable = false, writable = false) {
    Object.defineProperty(target, name, { value, enumerable, writable });
}

function _printProps(obj, prefix = '') {
    if (typeof _DEV_ === 'undefined' || !_DEV_) {
        return;
    }
    console.log(prefix + "{");
    for (const prop in obj) {
        // noinspection JSUnfilteredForInLoop
        let p = obj[prop];
        let value = typeof p === 'function'
            ? '[function]'
            : typeof p === 'object'
                ? JSON.stringify(p)
                : p;
        // noinspection JSUnfilteredForInLoop
        console.log(" ", prop, ":", value);
    }
    console.log("}");
}

// ret nullable
function _getPageByNode(node) {
    let pageId = node._getRootNode()._getPid();
    return getPage(pageId);
}

/**
 * 数据结构是有序树，虚拟节点 VNode ，一般作为终端节点，
 * 而 {@link VNodeGroup} 一般作为非终端节点。
 */
class VNode {
    #parent;
    #defaultPid;

    /**
     * @param nodeName {string} 节点名称，子类必须提供
     */
    constructor(nodeName) {
        if (typeof nodeName !== 'string') {
            throw new TypeError('need nodeName');
        }
        this.style = {};
        this.#defaultPid = 0;
        Object.defineProperty(this, "pid", {
            get: () => {
                if (this.#parent !== undefined) {
                    return this.#parent.id;
                }
                return this.#defaultPid;
            },
            enumerable: true
        });
        //只读的
        _defineProp(this, "id", genId(), true);
        _defineProp(this, "name", nodeName, true);
    }

    /**
     * 获取当前节点 id
     * @returns {number} 当前节点 id
     */
    getId() {
        return this.id;
    }

    /**
     * 设置当前节点的 style
     * @param style {object} 当前节点的 flex 主题
     */
    setStyle(style) {
        this.style = style;
        this._updateMyself();
    }

    /**
     * 设置点击事件回调
     * @param cb {function} 回调方法
     */
    setOnTap(cb) {
        this.onClick = cb;
        this._updateMyself();
    }

    /**
     * 设置触摸事件回调方法
     * @param cb {function}
     */
    setOnTouch(cb) {
        this.onTouch = cb;
        this._updateMyself();
    }

    setVisibility(v) {
        this.visibility = v;
        this._updateMyself();
    }

    /**
     * 获取父节点
     * @returns {VNode|undefined}
     */
    getParent() {
        return this.#parent;
    }

    _getPid() {
        return this.pid;
    }

    //设置默认的pid，如果 parent == undefined ，返回。
    _setPid(defaultPid) {
        this.#defaultPid = defaultPid;
    }

    _setParent(parent) {
        this.#parent = parent;
    }

    _getRootNode() {
        if (typeof this.#parent !== 'undefined') {
            return this.#parent._getRootNode();
        }
        return this;
    }

    //每次更新了自己的属性，需要同步数据到 UI 框架内部。
    _updateMyself() {
        let p = _getPageByNode(this);
        if (p) {
            p._updateNode(this);
        }
    }

    //执行节点上的方法
    _invoke(method, args) {
        let p = _getPageByNode(this);
        if (p) {
            p._invoke({ method, args });
        }
    }
}

/**
 * 虚拟节点，允许有多个子节点。
 */
class VNodeGroup extends VNode {
    #children;

    /**
     * @param nodeName {string} 节点名称，子类必须提供
     */
    constructor(nodeName) {
        super(nodeName);
        this.#children = [];
    }

    /**
     * 增加子节点 child
     * @param {VNode} child
     */
    appendChild(child) {
        const prevParent = child.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        child._setParent(this);
        this.#children.push(child);
        this._createNode(child);
    }

    /**
     * 移除子节点 child
     * @param {VNode} child
     */
    removeChild(child) {
        const index = this.#children.indexOf(child);
        if (index >= 0) {
            this.#children.splice(index, 1);
            child._setParent(undefined);
            this._removeNode(child);
        }
    }

    /**
     * 移除所有的子节点
     */
    removeAll() {
        for (const child of this.#children) {
            child._setParent(undefined);
            this._removeNode(child);
        }
        this.#children.length = 0;
    }

    /**
     * 在 existingChild 之前增加 child
     * @param {VNode} child
     * @param {VNode} existingChild
     */
    insertBefore(child, existingChild) {
        const prevParent = child.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        const index = this.#children.indexOf(existingChild);
        if (index >= 0) {
            const arr = this.#children.splice(index, this.#children.length - index);
            this.#children.push(child);
            this.#children.push(...arr);
            child._setParent(this);

            this._createNode(child, index);
        }
    }

    /**
     * 替换 oldChild 为 newChild
     * @param {VNode} newChild
     * @param {VNode} oldChild
     */
    replaceChild(newChild, oldChild) {
        const prevParent = newChild.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        const index = this.#children.indexOf(oldChild);
        if (index >= 0) {
            this.#children.splice(index, 1, newChild);

            oldChild._setParent(undefined);
            this._removeNode(oldChild);

            newChild._setParent(this);
            this._createNode(newChild, index);
        }
    }

    _createNode(child, index = -1) {
        let p = _getPageByNode(this);
        if (p) {
            p._createNode(child, index);
        }
    }

    _removeNode(child) {
        let p = _getPageByNode(this);
        if (p) {
            p._removeNode(child);
        }
    }

    _getChildren() {
        return this.#children;
    }
}

/**
 * FlexBox Layout 类型的 VNodeGroup
 */
class FlexNode extends VNodeGroup {
    constructor() {
        super('flex');
    }
}

/**
 * 文字节点，用来显示文字。
 */
class TextNode extends VNode {
    constructor() {
        super('text');
        this.text = undefined;
    }

    /**
     * 设置文本显示内容
     * @param {string} txt
     */
    setText(txt) {
        this.text = txt;
        this._updateMyself();
    }

    /**
     * 设置文字颜色
     * @param {string|number} color
     */
    setTextColor(color) {
        this.textColor = color;
        this._updateMyself();
    }

    /**
     * 设置文字权重，枚举值：'start' | 'left' | 'top' | 'right' | 'end' | 'bottom' | 'center'
     * @param {string} gravity
     */
    setGravity(gravity) {
        this.gravity = gravity;
        this._updateMyself();
    }

    /**
     * 设置文字大小
     * @param {string|number} textSize
     */
    setTextSize(textSize) {
        this.textSize = textSize;
        this._updateMyself();
    }
}

/**
 * 图片节点
 */
class ImageNode extends VNode {
    constructor() {
        super('image');
    }

    /**
     * 设置图片 url
     * @param url
     */
    setUrl(url) {
        this.url = url;
        this._updateMyself();
    }
}

//以广度优先遍历所有的 VNode 节点，必须是广度遍历。
function _traversalVNodes(nodeArray, consumer) {
    let queue = [];
    queue.push(...nodeArray);
    while (queue.length !== 0) {
        let item = queue.shift();
        if (!(item instanceof VNode)) {
            console.log("not an vnode:", JSON.stringify(item));
        }
        consumer(item);
        if (item instanceof VNodeGroup) {
            queue.push(...item._getChildren());
        }
    }
}

//模板的抽象类，子类需提供 children
class Temp {

    #isAdded;

    constructor() {
        //模版id
        _defineProp(this, "id", genId(), true);
        _defineProp(this, "name", 'view-temp', true);

        this.#isAdded = false;
    }

    setStyle(style) {
        this._checkStatus();
        this.style = style;
    }

    getId() {
        return this.id;
    }

    _setIsAdded(isAdded) {
        this.#isAdded = isAdded;
    }

    _checkStatus() {
        if (this.#isAdded) {
            throw new TypeError('can not update template when added');
        }
    }
}

/**
 * 一个可自定义 children 的模板，children 也可以被模板化。
 * 用来解决通过 data 来自定义多个 children 的情况。
 */
class CTemplate extends Temp {
    constructor(c) {
        super();
        this.children = c;
    }
}

// 在 child 中添加 值为 ${prop} 可作为占位。
// 模板不需要实时更新。
// 模版也要求严格控制 enumerable 属性。

/**
 * View 模版。模版中，使用 `${`+ prop +`}` 来标记模版中哪些属性的值是可以更改的，
 * 其中，`prop` 与 {@link VTempData#setData} 的 prop 需要一一对应。
 * <p>
 * 注意：需要在调用 {@link addTemplate} 前就设置好模版的状态。
 */
class VTemplate extends Temp {
    //模版的直接子节点，（其pid都等于模版id）
    #nodes;

    constructor() {
        super();
        this.#nodes = [];
        //模版的所有节点，必须以广度遍历排序。
        Object.defineProperty(this, "children", {
            get: () => {
                let cds = [];
                _traversalVNodes(this.#nodes, (item) => {
                    _printProps(item);
                    cds.push(item);
                });
                return cds;
            },
            enumerable: true
        });
    }


    appendChild(node) {
        this._checkStatus();
        const prevParent = node.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        //需要这里设置。
        node._setPid(this.getId());
        this.#nodes.push(node);
    }

    removeChild(child) {
        this._checkStatus();
        const index = this.#nodes.indexOf(child);
        if (index >= 0) {
            this.#nodes.splice(index, 1);
            child._setPid(0);
        }
    }

    removeAll() {
        this._checkStatus();
        for (const child of this.#nodes) {
            child._setPid(0);
        }
        this.#nodes.length = 0;
    }

    insertBefore(child, existingChild) {
        this._checkStatus();
        const prevParent = child.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        const index = this.#nodes.indexOf(existingChild);
        if (index >= 0) {
            const arr = this.#nodes.splice(index, this.#nodes.length - index);
            this.#nodes.push(child);
            this.#nodes.push(...arr);
            child._setPid(this.getId());
        }
    }

    replaceChild(newChild, oldChild) {
        this._checkStatus();
        const prevParent = newChild.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        const index = this.#nodes.indexOf(oldChild);
        if (index >= 0) {
            this.#nodes.splice(index, 1, newChild);
            oldChild._setPid(0);
            newChild._setPid(this.getId());
        }
    }

    _getChildren() {
        return this.children;
    }
}

/**
 * 模版的实例数据节点，参见 {@link VTemplate}。
 */
class VTempData extends VNode {
    constructor(templateId) {
        super('view-data');
        if (typeof templateId !== 'number') {
            throw new TypeError('VTempData must provide pageId');
        }
        //typeRef 不可变。如果想换，直接delete这个node就可以了。
        _defineProp(this, "typeRef", templateId, true);
        this.data = {}
    }

    /**
     * 设置模版的实例数据
     * @param data
     */
    setData(data) {
        this.data = data;
        this._updateMyself();
    }
}

/**
 * {@link ListViewNode} 的子节点，是 {@link VTempData} 的子类。
 */
class ListItemNode extends VTempData {
    constructor(templateId) {
        super(templateId);
        this.spanCount = 1;
    }

    setSpanCount(count) {
        this.spanCount = count;
        this._updateMyself();
    }
}

class ListViewNode extends VNodeGroup {
    constructor() {
        super('list-view');
        this.listType = 'linear';
        this.scrollDirection = 'vertical';
        this.spanCount = 12;
    }

    setListType(lt) {
        this.listType = lt;
        this._updateMyself();
    }

    setScrollDirection(sd) {
        this.scrollDirection = sd;
        this._updateMyself();
    }

    setSpanCount(count) {
        this.spanCount = count;
        this._updateMyself();
    }

    scrollToPosition(position, smooth = false) {
        this._invoke('scrollToPosition', [position, smooth]);
    }

    scrollTo(x, y) {
        this._invoke('scrollTo', [x, y]);
    }

    scrollBy(dx, dy) {
        this._invoke('scrollBy', [dx, dy]);
    }

}

/**
 * VPageNode 是一个特殊的 VNode，它让一个 VPage 可以像一个 VNode 一样组织进另一个 VPage。
 * 这填补了 VTemplate 不能动态管理的设计空白。
 */
class VPageNode extends VNode {
    constructor() {
        super('page-ref');
        this.typeRef = 0;
    }

    setTypeRef(pageId) {
        if (typeof pageId !== 'number') {
            throw new TypeError('VPageNode typeRef must be number type');
        }
        if (pageId !== this.typeRef) {
            this.typeRef = pageId;
            this._updateMyself();
        }
    }
}

class MarkdownNode extends VNode {

    constructor() {
        super('md-view');
    }

    setLinkResolver(linkResolver) {
        this.linkResolver = linkResolver;
        this._updateMyself();
    }

    setMarkdown(md) {
        this.md = md;
        this._updateMyself();
    }
}

class ScrollViewNode extends VNodeGroup {
    constructor() {
        super('scroll-view');
    }

    setScrollDirection(sd) {
        this.scrollDirection = sd;
        this._updateMyself();
    }
}

/**
 * VPage 在 JSKit APP 中表示一个页面。
 * <p>
 * {@link https://jskitapp.com/api/ui.html#vpage-状态机}
 * <p>
 * see {@link VPageNode}
 */
class VPage {
    #pageId;

    #children;

    #isLoaded;

    #onEvent;

    #onPreLoad;
    #onLoaded;

    #onShow;
    #onHide;

    #onPreUnload;
    #onUnloaded;

    constructor() {
        this.#pageId = genId();
        this.#children = [];

        this.#isLoaded = false;

        const onLoadFn = (p) => {
            //onPreLoad
            if (typeof this.#onPreLoad === 'function') {
                this.#onPreLoad();
            }

            //onLoad
            this.#isLoaded = true;
            _traversalVNodes(this.#children, (item) => {
                _printProps(item);
                p.createNode(item);
            });

            //onLoaded
            if (typeof this.#onLoaded === 'function') {
                this.#onLoaded();
            }
        };

        _defineProp(this, "onLoad", (p) => {
            try {
                onLoadFn(p);
            } catch (e) {
                console.log(e, e.stack);
            }
        }, true);

        _defineProp(this, "onShow", (p) => {
            if (typeof this.#onShow === 'function') {
                try {
                    this.#onShow();
                } catch (e) {
                    console.log(e, e.stack);
                }
            }
        }, true);

        _defineProp(this, "onHide", (p) => {
            if (typeof this.#onHide === 'function') {
                try {
                    this.#onHide();
                } catch (e) {
                    console.log(e, e.stack);
                }
            }
        }, true);

        const onUnloadFn = (p) => {
            //onPreUnLoad
            if (typeof this.#onPreUnload === 'function') {
                this.#onPreUnload();
            }

            //onUnload
            this.#isLoaded = false;

            //onUnloaded
            if (typeof this.#onUnloaded === 'function') {
                this.#onUnloaded();
            }
        }

        _defineProp(this, "onUnload", (p) => {
            try {
                onUnloadFn(p);
            } catch (e) {
                console.log(e, e.stack);
            }
        }, true);

        _defineProp(this, "onEvent", (name, args) => {
            if (typeof this.#onEvent === 'function') {
                try {
                    return this.#onEvent(name, args);
                } catch (e) {
                    console.log(e, e.stack);
                    return false;
                }
            }
            return false;
        }, true);
    }

    /**
     * 获取 pageId
     * @returns {number} pageId
     */
    getPageId() {
        return this.#pageId;
    }

    setStyle(style) {
        this.style = style;
        if (this.#isLoaded) {
            this._updateNode({
                id: this.#pageId,
                style: this.style
            });
        }
    }

    setOnEvent(onevent) {
        this.#onEvent = onevent;
    }

    setOnPreLoad(cb) {
        this.#onPreLoad = cb;
    }

    setOnLoaded(cb) {
        this.#onLoaded = cb;
    }

    setOnShow(cb) {
        this.#onShow = cb;
    }

    setOnHide(cb) {
        this.#onHide = cb;
    }

    setOnPreUnload(cb) {
        this.#onPreUnload = cb;
    }

    setOnUnloaded(cb) {
        this.#onUnloaded = cb;
    }

    appendChild(node) {
        const prevParent = node.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        node._setPid(this.#pageId);
        this.#children.push(node);

        this._createNode(node);
    }

    removeChild(child) {
        const index = this.#children.indexOf(child);
        if (index >= 0) {
            this.#children.splice(index, 1);
            child._setPid(0);
            this._removeNode(child);
        }
    }

    removeAll() {
        for (const child of this.#children) {
            child._setPid(0);
            this._removeNode(child);
        }
        this.#children.length = 0;
    }

    insertBefore(child, existingChild) {
        const prevParent = child.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        const index = this.#children.indexOf(existingChild);
        if (index >= 0) {
            const arr = this.#children.splice(index, this.#children.length - index);
            this.#children.push(child);
            this.#children.push(...arr);
            child._setPid(this.#pageId);

            this._createNode(child, index);
        }
    }

    replaceChild(newChild, oldChild) {
        const prevParent = newChild.getParent();
        if (typeof prevParent !== 'undefined') {
            throw new TypeError('already has parent');
        }
        const index = this.#children.indexOf(oldChild);
        if (index >= 0) {
            this.#children.splice(index, 1, newChild);

            oldChild._setPid(0);
            this._removeNode(oldChild);

            newChild._setPid(this.#pageId);
            this._createNode(newChild, index);
        }
    }

    _getChildren() {
        return this.#children;
    }

    _createNode(node, index = -1) {
        if (this.#isLoaded) {
            const p = ui.getPage(this.#pageId);
            if (p) {
                p.createNode(node, index);
                //如果这个 child 还有子 view，需要继续创建
                if (node instanceof VNodeGroup) {
                    _traversalVNodes(node._getChildren(), (item) => {
                        _printProps(item);
                        p.createNode(item);
                    });
                }
            }
        }
        return this.#isLoaded;
    }

    _updateNode(node) {
        if (this.#isLoaded) {
            const p = ui.getPage(this.#pageId);
            if (p) {
                p.updateNode(node);
            }
        }
        return this.#isLoaded;
    }

    _removeNode(node) {
        if (this.#isLoaded) {
            const p = ui.getPage(this.#pageId);
            if (p) {
                p.removeNode(node.getId());
            }
        }
        return this.#isLoaded;
    }

    _invoke(method, args) {
        if (this.#isLoaded) {
            const p = ui.getPage(this.#pageId);
            if (p) {
                p.invoke({ method, args });
            }
        }
        return this.#isLoaded;
    }
}

/**
 * 添加的模版是全局的，能够在所有 VPage 里共用。
 * @param template {VTemplate} 模版对象
 */
function addTemplate(template) {
    template._setIsAdded(true);
    ui.addTemplate(template);
}

/**
 * 可以通过模版 id 移除对应的 VTemplate
 * @param templateId {number} templateId
 */
function removeTemplate(templateId) {
    ui.removeTemplate(templateId)
}

//用来缓存添加过的 VPage
const idPagerMap = new Map();

/**
 * 添加 VPage。添加 VPage 只是向 UI 框架注册了页面，添加的 VPage 并不会立刻被渲染成页面。
 * 只在调用了 VPage 路由 {@link https://jskitapp.com/api/ui.html#vpage-路由 } 的相关方法才会进入渲染流程。
 * @param vpage {VPage} 要添加的 VPage
 */
function addPage(vpage) {
    idPagerMap.set(vpage.getPageId(), vpage);
    ui.addPage(vpage.getPageId(), vpage);
}

/**
 * 通过 pageId 移除某个已经添加的 vpage
 * @param pageId {number} 要移除的 pageId
 */
function removePage(pageId) {
    idPagerMap.delete(pageId);
    ui.removePage(pageId);
}

/**
 * 通过 pageId 获取某个已添加的 vpage
 * @param pageId 通过 pageId 获取 对应的 VPage
 * @returns {VPage|undefined} 返回对应的 VPage，如果没有，返回 undefined。
 */
function getPage(pageId) {
    return idPagerMap.get(pageId);
}

// 路由相关方法

/**
 * 加载某个页面。VPage 栈如果不为空，将会把当前栈中的所有 VPage 转换至 `UNLOAD` 状态，
 * 再将此指定页面转换至 `SHOWED` 状态。
 * @param vpage {VPage} 要加载的页面，必须得是已经 {@link addPage} 添加了的。
 */
function loadPage(vpage) {
    ui.loadPage({ id: vpage.getPageId() });
}

/**
 * 如果指定页面在当前栈中不存在：保留当前页面，跳转到应用内的某个页面。
 * 即，如果存在当前页面，当前页面会转至 `HIDDEN` 状态，然后加载指定页面到 `SHOWED` 状态。
 *
 * 如果指定页面在当前栈中已存在：栈中当前页面之前的页面会转至 `UNLOAD` 状态，再将当前页面转至 `SHOWED` 状态。
 * @param vpage {VPage} 要打开的页面，必须得是已经 {@link addPage} 添加了的。
 */
function navigateTo(vpage) {
    ui.navigateTo({ id: vpage.getPageId() });
}

/**
 * 如果指定页面在当前栈中不存在：关闭当前页面，跳转到应用内的某个页面。
 * 即，如果存在当前页面，当前页面会转至 `UNLOAD` 状态，然后加载指定页面到 `SHOWED` 状态。
 *
 * 如果指定页面在当前栈中已存在：栈中当前页面之前的页面会转至 `UNLOAD` 状态，再将当前页面转至 `SHOWED` 状态。
 * @param vpage {VPage} 要重定向的页面，必须得是已经 {@link addPage} 添加了的。
 */
function redirectTo(vpage) {
    ui.redirectTo({ id: vpage.getPageId() });
}

/**
 * 关闭当前页面，返回上一页面或多级页面。
 * @param delta {number} 返回的页面数，如果 delta 大于现有页面数，则返回到首页。
 */
function navigateBack(delta = 1) {
    ui.navigateBack({ delta });
}

//执行全局方法，这个与 VPage#_invoke 有所区别。
function _invokeAction(method, args) {
    ui.invoke({ method, args });
}

/**
 * 显示 Console 页面。
 */
function showConsole() {
    _invokeAction('showConsole');
}

export {
    VNode,
    VNodeGroup,
    FlexNode,
    TextNode,
    ImageNode,
    VTemplate,
    CTemplate,
    VTempData,
    ListItemNode,
    ListViewNode,
    VPageNode,
    MarkdownNode,
    ScrollViewNode,
    VPage,
    addTemplate,
    removeTemplate,
    addPage,
    getPage,
    removePage,
    loadPage,
    navigateTo,
    redirectTo,
    navigateBack,
    showConsole,
}
