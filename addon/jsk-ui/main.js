import * as ui from "ui";
import {addPage, loadPage, TextNode, VPage} from "jsk-ui";

//TODO 提供一个更加完整的 jsk-ui 使用示例。

//开启此项目的 DEV 模式
global._DEV_ = true;

const text = new TextNode();

text.setText("Hello World");
text.setStyle({
    flexGrow: 1,
});
text.setTextSize('17sp');
text.setGravity('center');
text.setOnTap(() => {
    text.setText("Hello JSK-UI");
    ui.toast("Hello jsk-ui");
});


const page = new VPage();

page.appendChild(text);
addPage(page);

loadPage(page);
