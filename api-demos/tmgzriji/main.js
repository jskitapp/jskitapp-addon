import * as ui from "ui";
import { createRequire } from "cjs";
import {
    TextNode, VPage, FlexNode, addPage, loadPage
} from 'jsk-ui';

console.log("hello world");

const require = createRequire(Deno.cwd());

const axios = require('axios');

const text = new TextNode();

//https://api.muxiaoguo.cn/api/dujitang
//https://api.muxiaoguo.cn/api/tiangourj
async function getTianGouRiJi() {
    try {
        const response = await axios.get('https://api.muxiaoguo.cn/api/tiangourj');
        console.log(JSON.stringify(response));

        if (response.data.data.comment === undefined && (typeof response.data.msg === 'string')) {
            ui.toast(response.data.msg);
            return;
        }
        text.setText(response.data.data.comment);
        ui.toast("舔狗日记已刷新～");
    } catch (error) {
        console.log(JSON.stringify(error));
        ui.toast("http 接口错误");
    }
}

text.setTextSize('17sp');
text.setOnTap(() => {
    getTianGouRiJi();
});

text.setStyle({
    flexShrink: 1,
    alignItems: "center",
    margin: '20dp',
});

let flex = new FlexNode();
flex.setStyle({
    width: "100%",
    height: "56dp",
    flexDirection: "row",
    justifyContent: "center",
});

let title = new TextNode();
title.setText("舔狗日记");
title.setTextSize('17sp');
title.setGravity('center');

flex.appendChild(title);

let page = new VPage();
page.appendChild(flex);
page.appendChild(text);

addPage(page);

loadPage(page);

getTianGouRiJi();
