import * as ui from 'ui';
import {addPage, FlexNode, loadPage, TextNode, VPage, VPageNode} from 'jsk-ui'

console.log("hello world");
//global._DEV_ = true;

class OneTextPage extends VPage {
    constructor(txt) {
        super();

        this.setStyle({
            flexGrow: 1,
            borderColor: "red",
            borderWidth: "2dp"
        });
        const text = new TextNode;

        text.setStyle({
            flexGrow: 1,
            borderColor: "red",
            borderWidth: "2dp"
        });

        text.setText(txt);
        text.setGravity('center');

        this.appendChild(text);
    }
}

const page1 = new OneTextPage('page1');
const page2 = new OneTextPage('page2');
const page3 = new OneTextPage('page3');

addPage(page1);
addPage(page2);
addPage(page3);

let toasted = false;

const homePage = new VPage();
homePage.setStyle({
    flexDirection: "column",
    alignItems: "stretch"
});

homePage.setOnEvent((name, args) => {
    let oldv = toasted;
    if (!oldv) {
        ui.toast('再按一次退出');
        toasted = true;
        setTimeout(() => {
            toasted = false;
        }, 3000);
    }
    return !oldv;
});

class ContentNode extends VPageNode {
    constructor(pageId) {
        super();
        this.setTypeRef(pageId)
        this.setStyle({
            flexGrow: 1,
        });
    }
}

let lastContent = new ContentNode(page1.getPageId());
let lastIndex = 1;
homePage.appendChild(lastContent);

const bottomBar = new FlexNode();

bottomBar.setStyle({
    height: "56dp",
    flexDirection: "row",
});

homePage.appendChild(bottomBar);

class TabNode extends TextNode {
    constructor(tabName) {
        super();
        this.setStyle({
            flexGrow: 1,
            borderColor: "blue",
            borderWidth: "2dp"
        });

        this.setText(tabName);

        this.setGravity("center");
    }
}

const tab1 = new TabNode('tab1');
const tab2 = new TabNode('tab2');
const tab3 = new TabNode('tab3');

tab1.setOnTap(() => {
    if (lastIndex !== 1) {
        lastContent.setTypeRef(page1.getPageId());
        lastIndex = 1;
    }
});

tab2.setOnTap(() => {
    if (lastIndex !== 2) {
        lastContent.setTypeRef(page2.getPageId());
        lastIndex = 2;
    }
});

tab3.setOnTap(() => {
    if (lastIndex !== 3) {
        lastContent.setTypeRef(page3.getPageId());
        lastIndex = 3;
    }
});

bottomBar.appendChild(tab1);
bottomBar.appendChild(tab2);
bottomBar.appendChild(tab3);

addPage(homePage);

loadPage(homePage);
