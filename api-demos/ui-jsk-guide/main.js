import * as ui from "ui";
import { File, FileReader } from 'file';
import { addPage, FlexNode, loadPage, MarkdownNode, TextNode, VPage } from "jsk-ui";

let toasted = false;

const file = new File('./README.md');

async function loadMdContent() {
    return new FileReader(file).readString();
}


const toolbar = new FlexNode();
toolbar.setStyle({
    width: "100%",
    height: "56dp",
    flexDirection: "row",
    justifyContent: "center",
});

const title = new TextNode();
title.setStyle({
    flexGrow: 1,
});
title.setTextSize('17sp');
title.setGravity('center');
title.setText("JSKit APP Guide");

toolbar.appendChild(title);

const infoView = new MarkdownNode();
infoView.setStyle({
    flexShrink: 1,
    alignItems: "center",
    borderColor: "blue",
    flexDirection: "column",
    borderWidth: "2dp"
});
infoView.setLinkResolver((name, args) => {
    const link = args.link;
    console.log("on link:", link);
    if (link.startsWith('./')) {
        ui.toast("此链接类型暂未支持，只可在编辑器中打开")
        return true;
    }
    return false;
})


loadMdContent()
    .then(content => {
        infoView.setMarkdown(content);
    });

const page = new VPage();

page.appendChild(toolbar);
page.appendChild(infoView);

page.setOnEvent((name, args) => {
    let oldv = toasted;
    if (!oldv) {
        ui.toast('再按一次退出');
        toasted = true;
        setTimeout(() => {
            toasted = false;
        }, 3000);
    }
    return !oldv;
});

addPage(page);
loadPage(page);
